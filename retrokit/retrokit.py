# retrokit.py

from __future__ import with_statement
import random
import sys
import time
import pygame
from pygame.locals import *
from font import Font
from corefont import corefont
from fontimagecollection import FontCollection
from screen import Screen

# XXX why is this here? seems to be some experiments and demos... we already
# have a demo directory for that... Also, the code seems kinda old, e.g. it
# doesn't use Application? 
# XXX may be removed? if so, what will be the purpose of retrokit.py?

if __name__ == "__main__":

    print len(corefont.data)
    print corefont[0], corefont[32], corefont[33]

    NAVY = (0, 0, 0x80)
    WHITE = (0xFF, 0xFF, 0xFF)

    fi = FontImageCollection(corefont)
    for i in range(len(corefont.data)):
        fi.make_image(i, NAVY, WHITE)
    print fi.images.items()[0]

    pygame.init()

    screen = Screen(40, 25, 2)
    # scale must match font size, of course
    
    def demo_random_chars():
        while 1:
            for event in pygame.event.get():
                if event.type == QUIT:
                    sys.exit()

            # much faster than earlier attempts ^_^
            for x in range(40):
                for y in range(25):
                    screen.setxy(x, y, fi, random.randint(0, 255), NAVY, WHITE)

            pygame.display.update()

    def demo_show_core_font():
        x = y = 0
        for i in range(256):
            screen.setxy(x, y, fi, i, NAVY, WHITE)
            x += 1
            if x >= screen.width:
                x = 0; y += 1

        # test mappings
        chars = ['A', 'b', '9', '+', '[', 'up']
        for idx, c in enumerate(chars):
            screen.setxy(idx, 24, fi, c, NAVY, WHITE)
        
        while 1:
            for event in pygame.event.get():
                if event.type == QUIT:
                    sys.exit()
            pygame.display.update()

    demo_show_core_font()

