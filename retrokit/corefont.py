# corefont.py
# The core font is special; it is always loaded, and some modifications are
# made to it that aren't (automatically) done to other fonts.

import os
import font
import custom_chars
import fonttools
import charsets.c64

#_fn = os.path.join(os.path.dirname(os.path.abspath(__file__)), 
#      "charsets", "c64.bin")
corefont = font.Font()
corefont.add(charsets.c64.c64charset)

'''
# remove reverse characters, they're redundant
for i in range(128, 256):
    corefont.data[i] = corefont.data[i+128]
keys = [key for key in corefont.data.keys() if key >= 256]
for key in keys:
    del corefont.data[key]
'''

# 
# add custom characters.
# NOTE: since  these may be added, removed or changed over time, it is best to
# only refer to these character by NAME, rather than by number.

# FIXME: these need stored as list of strings
'''
custom_char_names = [name for name in dir(custom_chars)
                     if not name.startswith("_")]

max_key = max([key for key in corefont.data.keys() if type(key) is type(0)])
for i, name in enumerate(custom_char_names):
    lines = getattr(custom_chars, name).strip().split('\n')
    assert len(lines) == 8
    values = [fonttools.line_to_byte(line) for line in lines]
    new_key = max_key + 1 + i
    corefont.data[new_key] = values
    corefont.data[name] = values
    #print "Added:", name, new_key, values
'''

