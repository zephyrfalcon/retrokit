# fontimages.py

import pygame
import fontimage

class FontImageCollection:
    """ Manage a collection of font images. 

    Images are stored per (idx, fgcolor, bgcolor); in other words, there is a
    separate image for each color combination (that you choose to add). This
    takes up some space, but performs much faster than directly manipulating
    pixels. If memory is an issue, make sure to only add characters and colors
    that your program actually uses.
    """

    def __init__(self, font, scale=2):
        self.font = font # can be a Font class; in a pinch, CharSet works too
        self.scale = scale
        self.images = {}

    def make_image(self, idx, colors, add=True):
        chars = self.font[idx] # list of 8 strings of 8 chars each
        surface = pygame.Surface((8*self.scale, 8*self.scale))
        colors = tuple(sorted(colors)) # normalize
        d = dict(colors)
        # "plot" pixels in there
        for y in range(8):
            for x in range(8):
                c = chars[y][x]
                #bit_set = (c == "*") #chars[y] & (2**(7-x))
                color = d[c]
                for ix in range(self.scale):
                    for iy in range(self.scale):
                        px = x*self.scale + ix
                        py = y*self.scale + iy
                        surface.set_at((px, py), color)

        img = fontimage.FontImage(surface)

        # add to dictionary 
        if add:
            key = (idx, colors)
            self.images[key] = img

        return img

    def make_moving_image(self, indexes, colors, speed):
        """ Create a MovingImage from the specified indexes. The indexes must
            be present in self.font. The newly created image(s) will not be
            added to the index. """
        surfaces = [self.make_image(idx, colors, add=False).get_image() 
                    for idx in indexes]
        return fontimage.MovingImage(surfaces, speed)

    def get(self, idx, colors):
        colors = tuple(sorted(colors))
        try:
            # if image is not in dictionary, create it
            return self.images[idx, colors]
        except KeyError:
            # if image is not in dictionary, create it
            return self.make_image(idx, colors)

    def __getitem__(self, indicator):
        return self.get(*indicator)


