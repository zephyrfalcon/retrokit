# screen.py

import pygame
import types
import tools

class ScreenError(Exception): pass

class Screen:

    # Idea: What if we stored FontImage instances (or subclasses) rather than
    # (idx, colors)? We can add the idx and colors to FontImage for later
    # reference. This way, self._data is an array of FontImages+ only. And
    # we don't have to look up the image each time in FontImageCollection.

    def __init__(self, width=40, height=25, scale=2, use_scanlines=False):
        self.width = width
        self.height = height
        self.scale = scale
        self.use_scanlines = use_scanlines
        self.size = (width*8*scale, height*8*scale)
        self.screen = pygame.display.set_mode(self.size)
        # ^ returns a Surface object
        self._data = [[None for x in range(width)] for y in range(height)]

        self._scanlines = pygame.Surface(self.size).convert_alpha()
        # change these values to set the "strength"; e.g. (0,0,0,*) would
        # blend with black, creating the darkest scanlines
        self._scanlines.fill((64*3,64*3,64*3,255))
        for j in range(0, self.size[1], 2):
            print((0,j,self.size[0],j))
            self._scanlines.fill((255,255,255,255), (0, j, self.size[0], 1))

    def setxy(self, x, y, fi, idx, colors):
        """ Look up the given character (via idx) in FontImageCollection instance
            fi, using the given colors. Then store this image at position (x,y)
            and draw it on the screen. """
        colors = tuple(sorted(colors))
        try:
            self._data[y][x] = (idx, colors)
        except IndexError:
            raise ScreenError("Invalid coordinates: (%d,%d)" % (x, y))
        self._setxy(x, y, fi, idx, colors)

    def _setxy(self, x, y, fi, idx, colors):
        surface = fi.get(idx, colors).get_image()
        self.screen.blit(surface, (x*8*self.scale, y*8*self.scale))

    def setxy_direct(self, x, y, img):
        """ Like setxy(), but rather than looking up an image in
            FontImageCollection, we place an image directly. """
        # NOTE: Currently does not store the image; should it?
        self.screen.blit(img.get_image(), (x*8*self.scale, y*8*self.scale))

    def __setitem__(self, (x, y), obj):
        """ Sets a value in Screen's internal grid, but does NOT draw
            anything. Use with caution. """
        self._data[y][x] = obj

    def __getitem__(self, (x, y)):
        return self._data[y][x]

    def getxy(self, x, y, default=None):
        return self._data[y][x] or default

    def clear(self, fi, idx, colors):
        """ Clear the screen by filling it with the given character (looked up
            in fi via idx) in the given colors. """
        for y in range(self.height):
            for x in range(self.width):
                self.setxy(x, y, fi, idx, colors)

    def writexy(self, x, y, fi, colors, text, wrap=0):
        if type(text) in types.StringTypes:
            cx, cy = x, y
            for c in text:
                if cx >= self.width:
                    if wrap:
                        cx = 0; cy += 1
                    else:
                        return # don't write anything else
                self.setxy(cx, cy, fi, c, colors)
                cx += 1
            return cx, cy
        elif isinstance(text, tools.Symbol):
            self.setxy(x, y, fi, text.name, colors)
            return x+1, y
        else:
            # assumed to be a sequence or iterator
            cx, cy = x, y
            for s in text:
                cx, cy = self.writexy(cx, cy, fi, colors, s, wrap)
            return cx, cy

    def fill(self, x1, y1, x2, y2, fi, idx, colors):
        for y in range(y1, y2+1):
            for x in range(x1, x2+1):
                self.setxy(x, y, fi, idx, colors)

    def draw(self, fi):
        # XXX untested
        for y in range(self.height):
            for x in range(self.width):
                obj = self._data[y][x]
                if type(obj) is types.TupleType:
                    idx, colors = obj
                    self._setxy(x, y, fi, idx, colors)
                elif isinstance(obj, fontimage.FontImage):
                    self.setxy_direct(x, y, obj)
                elif obj is None:
                    pass
                else:
                    raise ScreenError("Unknown object at (x,y): %s" % obj)

    def draw_scanlines(self):
        self.screen.blit(self._scanlines, (0,0), special_flags=pygame.BLEND_RGBA_MULT)

