# fonttools.py
#
# TODO: Add functions to rotate, mirror, reverse characters

def line_to_byte(s):
    """ Takes a string of 8 characters, each of which must be either a dot (.)
        or a hash sign (#). Converts the string to a numerical value, where
        each position represents a byte (dots are 0, hashes are 1).

        E.g.  ........       => 0
              ....#.#.       => 10
              #......#       => 129
    """

    assert len(s) == 8
    value = 0
    for c in s:
        assert c in ".#"
        value = value * 2 + (0 if c == '.' else 1)
    return value

def reverse(values):
    return [255-byte for byte in values]

