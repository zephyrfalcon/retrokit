# fontimage.py

# FontImage and subclasses represent various ways to store images (8x8 pixels,
# usually characters). 

class FontImage:
    """ The most basic class of image. It stores a PyGame Surface object and
        returns this. The image does not change. """
    def __init__(self, surface):
        self.surface = surface 
    def get_image(self):
        return self.surface

# There are two different objects:
# 1. A collection of characters (as lists of strings), representing the
# various states of the moving character/image. (This will be used to create
# #2. It will not be directly put on a Screen.)
# 2. A collection of actual *images* based on these characters, together with
# the currently selected image, and an attribute indicating the speed with
# which the current image changes. (This is what will be used to put the image
# on a Screen)

class MovingImage(FontImage):
    def __init__(self, surfaces, speed=0):
        self.surfaces = surfaces
        self.speed = speed
        self.counter = 0
        self.current = 0
    def get_image(self):
        return self.surfaces[self.current]
    def move(self):
        self.current += 1
        if self.current >= len(self.surfaces):
            self.current = 0
    def tick(self):
        """ Call this every clock tick. """
        self.counter += 1
        if self.counter >= self.speed:
            self.move()
            self.counter = 0

class ConditionalImage(FontImage):
    pass

