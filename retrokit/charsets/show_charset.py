#!/usr/bin/env python
# show_charset.py

import sys

def display_character(index, bytes):
    print "#%d:" % index
    for byte in bytes:
        s = ""
        for i in range(8)[::-1]:
            s += "a" if byte & 2**i else "_"
        print s
    print

def show_charset(filename):
    with open(filename, 'rb') as f:
        data = [ord(x) for x in f.read()]
    i = 0
    while data:
        chunk, data = data[:8], data[8:]
        display_character(i, chunk)
        i += 1
    print "OK"

if __name__ == "__main__":

    filename = sys.argv[1]
    show_charset(filename)

