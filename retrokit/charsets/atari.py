# atari.py

# Note: The index of characters is based on where they appear in the binary,
# rather than on ATASCII.

import charset

import sys, os
_whereami = os.path.dirname(os.path.abspath(__file__))
_atari_bin = os.path.join(_whereami, "atari800.bin")

ataricharset = charset.CharSet()
ataricharset.load(_atari_bin)

import string
for idx, c in enumerate(string.ascii_lowercase):
    ataricharset.aliases[c] = idx + 97
    ataricharset.aliases[c.upper()] = idx + 33
for idx, c in enumerate(string.digits):
    ataricharset.aliases[c] = idx + 16

ataricharset.aliases.update({
    'space': 0,
    ' ': 0,
    'exclamation-mark': 1,
    '!': 1,
    'quote': 2,
    '"': 2,
    'hash': 3,
    '#': 3,
    'string': 4,
    '$': 4,
    'dollar-sign': 4,
    'percent': 5,
    '%': 5,
    'percentage': 5,
    'ampersand': 6,
    '&': 6,
    'apostrophe': 7,
    "'": 7,
    'left-parenthesis': 8,
    '(': 8,
    'right-parenthesis': 9,
    ')': 9,
    'asterisk': 10,
    'star': 10,
    '*': 10,
    'plus': 11,
    '+': 11,
    'comma': 12,
    ',': 12,
    'minus': 13,
    '-': 13,
    'period': 14,
    '.': 14,
    'slash': 15,
    '/': 15,
    'colon': 26,
    ':': 26,
    'semicolon': 27,
    ';': 27,
    'less-than': 28,
    '<': 28,
    'equals': 29,
    '=': 29,
    'greater-than': 30,
    '>': 30,
    'question-mark': 31,
    '?': 31,
    '@': 32,
    'left-bracket': 59,
    '[': 59,
    'backslash': 60,
    '\\': 60,
    'right-bracket': 61,
    ']': 61,
    'caret': 62,
    '^': 62,
    '_': 63,
    'underscore': 63,
    'heart': 64,
    '/': 70,
    'slash': 70,
    '\\': 71,
    'backslash': 71,
    'clubs': 80,
    'ball': 84,
    'ball-closed': 84,
    'et': 91,
    'arrow-up': 92,
    'arrow-down': 93,
    'arrow-left': 94,
    'arrow-right': 95,
    'diamonds': 96,
    'spades': 123,
    'crooked-arrow': 125,
    'triangle-left': 126,
    'triangle-right': 127,
})

