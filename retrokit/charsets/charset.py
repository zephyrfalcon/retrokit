# charset.py

class CharSet:
    
    def __init__(self):
        self.data = [] # list of characters
        self.aliases = {}

    def __getitem__(self, name):
        if type(name) is int:
            return self.data[name]
        else:
            return self.data[self.aliases[name]]

    def load(self, filename):
        self.data = []
        with open(filename, 'rb') as f:
            everything = f.read()
        while len(everything) >= 8:
            chunk, everything = everything[:8], everything[8:]
            # XXX convert chars to bytes, then to list of "pixels"
            chars = [convert(c) for c in chunk]
            self.data.append(chars)

    def copy(self):
        other = CharSet()
        other.data = self.data[:]
        other.aliases = self.aliases.copy()
        return other

def convert(c):
    byte = ord(c)
    chars = ""
    for i in range(8)[::-1]:
        chars += "a" if byte & 2**i else "_"
    return chars

