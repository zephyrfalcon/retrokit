# c64.py

# Note: The index of characters is based on where they appear in the binary,
# rather than on PETSCII order.

# NOTE: Later we can add e.g. the C16 charset, which will be mostly the same,
# but the font has been tweaked here and there. All the indexes should be the
# same though.

import charset

import sys, os
_whereami = os.path.dirname(os.path.abspath(__file__))
_c64_bin = os.path.join(_whereami, "c64.bin") # includes reverse variants

c64charset = charset.CharSet()
c64charset.load(_c64_bin)

import string
for idx, c in enumerate(string.ascii_lowercase):
    c64charset.aliases[c] = idx + 257
    c64charset.aliases[c.upper()] = idx + 321
for idx, c in enumerate(string.digits):
    c64charset.aliases[c] = idx + 304

c64charset.aliases.update({
    'space': 32,
    ' ': 32,
    'exclamation-mark': 33,
    '!': 33,
    'quote': 34,
    '"': 34,
    'hash': 35,
    '#': 35,
    'string': 36,
    '$': 36,
    'dollar-sign': 36,
    'percent': 37,
    '%': 37,
    'percentage': 37,
    'ampersand': 38,
    '&': 38,
    'apostrophe': 39,
    "'": 39,
    'left-parenthesis': 40,
    '(': 40,
    'right-parenthesis': 41,
    '(': 41,
    'asterisk': 42,
    'star': 42,
    '*': 42,
    'plus': 43,
    '+': 43,
    'comma': 44,
    ',': 44,
    'minus': 45,
    '-': 45,
    'period': 46,
    '.': 46,
    'slash': 47,
    '/': 47,
    'colon': 58,
    ':': 58,
    'semicolon': 59,
    ';': 59,
    'less-than': 60,
    '<': 60,
    'equals': 61,
    '=': 61,
    'greater-than': 62,
    '>': 62,
    'question-mark': 63,
    '?': 63,
    '@': 0,
    'left-bracket': 27,
    '[': 27,
    'backslash': 60,
    '\\': 60,
    'right-bracket': 29,
    ']': 29,
    'pound': 28,
    'pound-sign': 28,
    'up-arrow': 30,
    'arrow-up': 30,
    'left-arrow': 31,
    'arrow-left': 31,
    'ball-closed': 81,
    'ball-open': 87,
    'spades': 65,
    'heart': 83,
    'clubs': 88,
    'diamonds': 90,
    'pi': 94,

    'square-root': 122+128,

    'top-right-rounded-corner': 73,
    'bottom-left-rounded-corner': 74,
    'bottom-right-rounded-corner': 75,
    'top-left-rounded-corner': 85,

    'diagonal-backslash': 77,
    'diagonal-slash': 78,

    'bottom-left-edge': 76,
    'top-left-edge': 79, 
    'top-right-edge': 80,
    'bottom-right-edge': 122,

    'bottom-left-square-corner': 109,
    'top-right-square-corner': 110,
    'top-left-square-corner': 112,
    'bottom-right-square-corner': 125,

    'horizontal-line': 64,
    'vertical-line': 93,

    'vertical-right': 107,
    'horizontal-up': 113,
    'horizontal-down': 114,
    'vertical-left': 115,

    'left-dark': 97,
    'bottom-dark': 98,

    'top-right-diagonal-dark': 95,
    'top-left-diagonal-dark': 96,

    'bottom-right-dark': 108,
    'bottom-left-dark': 123,
    'top-right-dark': 124,
    'top-left-dark': 125,

    'checkered': 127,

    'stripes-right': 105+128,
    'stripes-left': 95+128,
    'stipple': 94+128,
    'diagonal-cross': 86,
    'cross': 91,
})


