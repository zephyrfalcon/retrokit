# application.py

import sys
import pygame
from pygame.locals import *

class Application:

    def __init__(self, screen):
        self.screen = screen
        self.fps = 60
        self.setup()

    @classmethod
    def with_screen(cls, *args, **kwargs):
        """ Helper method to create screen and application in one. """
        import screen
        scr = screen.Screen(*args, **kwargs)
        return cls(scr)

    def setup(self):
        """ Override to add custom code to set up application. """
        pass

    def update(self):
        """ Called after every "clock tick". Override in subclass. """
        pass

    def mainloop(self):
        clock = pygame.time.Clock()
        while True:
            clock.tick(self.fps)
            for event in pygame.event.get():
                if event.type == QUIT:
                    sys.exit()
                elif event.type == KEYDOWN:
                    self.handle_key(event)
 
            #if self.screen.use_scanlines:
            #    self.screen.draw_scanlines()           
            self.update()

    def update_screen(self):
        """ Update the PyGame display, i.e. display any changes that were
            made. """
        if self.screen.use_scanlines:
            self.screen.draw_scanlines()           
        pygame.display.update()

    def redraw(self):
        """ Redraw the whole screen. """
        self.screen.clear()

    def handle_key(self, event):
        """ Handle a keypress. Override in subclass. """
        pass
  

