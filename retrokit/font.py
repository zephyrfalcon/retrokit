# font.py

from __future__ import with_statement

class Font:
    """ Holds an 8-bit character set. The characters can be taken from CharSet
        objects, and are indexed by name rather than by number. """

    def __init__(self):
        self.data = {}

    def __getitem__(self, index):
        return self.data[index]

    def __setitem__(self, index, char):
        # XXX sanity check for char?
        self.data[index] = char

    def add(self, charset, exclude=[]):
        """ Add all characters from <charset>, except those named in
            <exclude>. """  
        for name, idx in charset.aliases.items():
            if name in exclude:
                continue
            char = charset.data[idx]
            self.data[name] = char

    def add_some(self, charset, names):
        """ Add the given characters from <charset>. """
        for name in names:
            idx = charset.aliases[name]
            char = charset.data[idx]
            self.data[name] = char


