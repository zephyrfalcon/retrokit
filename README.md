This is retrokit, a toolkit based on PyGame for writing retro-looking
software.

Current status: very much under development!

(Much of the code is clumsy and/or old, and is currently being overhauled.)

### Requirements ###

* PyGame 1.9.2
* Python 2.7 (for now)

This should work on Windows, Linux and Mac OS X, but currently it has only
been tested on OS X. That should be remedied soon... >_>

### For now... ###

There is no installer or anything right now. Just install the requirements, check out the repo somewhere, then try to run things in the demo directory, e.g.

```
python demo/hello.py
```
