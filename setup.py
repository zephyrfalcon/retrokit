# setup.py

from distutils.core import setup
import retrokit

setup(name='retrokit',
      version=retrokit.__version__,
      description='...',
      author='Hans Nowak',
      author_email='...',
      url='...',
      packages=['retrokit', 'retrokit.charsets'],
      install_requires=['pygame>=1.9.2'],
      package_data={
          'binaries': ['retrokit/charsets/c64.bin',
                       'retrokit/charsets/atari800.bin']
          },
    )


