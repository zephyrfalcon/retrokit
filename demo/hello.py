# hello.py
# Simple hello world type demo.
# To show scanlines, run with command line argument 'scanlines'.

from __future__ import print_function
import os, sys
whereami = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.abspath(os.path.join(whereami, ".."))
sys.path.insert(0, parent_dir)

import random

from retrokit import *

BG = (0x00, 0x00, 0xC0)
FG = (0xC0, 0xC0, 0xC0)

def twocolor(fg, bg):
    return (("a", fg), ("_", bg))

myfont = font.Font()
myfont.add(charsets.ataricharset)

fi = FontImageCollection(myfont, scale=2)
for name in myfont.data.keys():
    fi.make_image(name, color(a=FG, _=BG))

C = colors.RetroKitColors

use_scanlines = 'scanlines' in sys.argv[1:]

class MyApp(Application):

    def setup(self):
        self.screen.clear(fi, " ", color(a=FG, _=BG))

        self.screen.writexy(0, 0, fi, color(a=FG, _=BG), "Hello world!")
        self.screen.writexy(0, 1, fi, color(a=FG, _=BG), 
                            "This is an example of the Atari font!")

        self.screen.writexy(0, 3, fi, color(a=C.yellow, _=BG),
          "We can use different colors... :)")

        self.screen.fill(0, 5, 39, 9, fi, " ", color(a=FG, _=C.black))
        self.screen.writexy(0, 6, fi, color(a=FG, _=C.black), 
          "Of course we can change the background color too! And there is a "\
          "primitive form of word wrap! Totally 80s!",
          wrap=True)

        self.screen.writexy(0, 11, fi, color(a=FG, _=BG), 
                            "Symbols are easily displayed:")
        self.screen.writexy(0, 12, fi, color(a=FG, _=BG), 
                            ["I ", Symbol("heart"), " cookies"])

        self.update_screen()

    def handle_key(self, event):
        if event.key == 27: # Escape
            sys.exit()

print("Press Escape to exit.")
app = MyApp.with_screen(40, 25, scale=2, use_scanlines=use_scanlines)
app.mainloop()


