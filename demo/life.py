# life.py
# Conway's Game of Life.
# Usage: python life.py [seed [starting_number]]

# With seed=1, even a starting number as low as 20 will cause massive
# proliferation after a few rounds...

import os, sys
whereami = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.abspath(os.path.join(whereami, ".."))
sys.path.insert(0, parent_dir)

import random

from retrokit import *

BG = (0x00, 0x00, 0xC0)
FG = (0xC0, 0xC0, 0xC0)

COLORS = (("a", FG), ("_", BG))

use_scanlines = 'scanlines' in sys.argv[1:]

fi = FontImageCollection(corefont, scale=2)
for i in ['ball-closed', ' ']:
    fi.make_image(i, COLORS)

def determine_params():
    args = [arg for arg in sys.argv[1:] if arg != 'scanlines']
    if args:
        seed = int(args[0])
    else:
        seed = 1
    if args[1:]:
        starting = int(args[1])
    else:
        starting = 50

    return seed, starting

ALIVE = 'ball-closed' # 81 #corefont['ball-closed']
DEAD = ' ' # 32 # corefont[' ']   # space

def calc_next_gen(screen):
    d = {}
    for x in range(screen.width):
        for y in range(screen.height):
            d[x,y] = is_alive(screen, x, y)
    return d

def is_alive(screen, x, y):
    """ Return True if the cell at (x, y) will be alive. """
    neighbors = 0
    for dx in (-1, 0, 1):
        for dy in (-1, 0, 1):
            try:
                value = screen.getxy(x+dx, y+dy)
            except IndexError:
                continue # outside of screen
            char, _ = value
            if char == ALIVE:
                neighbors += 1

    # a cell remains alive if it has 2 or 3 neighbors
    return neighbors in (2, 3)

class MyApp(Application):

    def setup(self):
        self.fps = 1 # update every second
        self.screen.clear(fi, DEAD, COLORS)

        seed, starting = determine_params()
        random.seed(seed)
        for i in range(starting):
            x = random.randint(0, self.screen.width-1)
            y = random.randint(0, self.screen.height-1)
            self.screen.setxy(x, y, fi, ALIVE, COLORS)

        self.update_screen()

    def update(self):
        updates = calc_next_gen(self.screen)
        for x in range(self.screen.width):
            for y in range(self.screen.height):
                cell = ALIVE if updates[x,y] else DEAD
                self.screen.setxy(x, y, fi, cell, COLORS)

        self.update_screen()

    def handle_key(self, event):
        if event.key == 27: # Escape
            sys.exit()

print "Press Escape to exit."
app = MyApp.with_screen(40, 25, scale=2, use_scanlines=use_scanlines)
app.mainloop()

