# randomchars.py

import os, sys
whereami = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.abspath(os.path.join(whereami, ".."))
sys.path.insert(0, parent_dir)

import random
from retrokit import *
from retrokit.charsets.c64 import c64charset

NAVY = (0x00, 0x00, 0x80)
WHITE = (0xFF, 0xFF, 0xFF)

COLORS = color(a=NAVY, _=WHITE)

use_scanlines = 'scanlines' in sys.argv[1:]

fi = FontImageCollection(c64charset, scale=2)
for i in range(len(c64charset.data)):
    fi.make_image(i, COLORS)

class MyApp(Application):

    def update(self):
        for x in range(self.screen.width):
            for y in range(self.screen.height):
                idx = random.randint(0, len(c64charset.data)-1)
                self.screen.setxy(x, y, fi, idx, COLORS)
        self.update_screen()

    def handle_key(self, event):
        # press any key to exit
        sys.exit()

app = MyApp.with_screen(40, 25, scale=2, use_scanlines=use_scanlines)
#app.fps = 1 # we can set this to change the speed
app.mainloop()

