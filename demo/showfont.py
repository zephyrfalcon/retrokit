# showfont.py

import os, sys
whereami = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.abspath(os.path.join(whereami, ".."))
sys.path.insert(0, parent_dir)

from retrokit import *

from retrokit.charsets.c64 import c64charset
charset = c64charset

use_scanlines = 'scanlines' in sys.argv[1:]
if 'atari' in sys.argv[1:]:
    from retrokit.charsets.atari import ataricharset
    charset = ataricharset

NAVY = (0x00, 0x00, 0x80)
WHITE = (0xFF, 0xFF, 0xFF)

# needs to be moved to retrokit itself
def color(**kwargs):
    return kwargs.items()

#COLORS = (("a", NAVY), ("_", WHITE))
COLORS = color(_=WHITE, a=NAVY)

fi = FontImageCollection(charset, scale=2)
for i in range(len(charset.data)):
    fi.make_image(i, COLORS)

screen = Screen(40, 25, scale=2, use_scanlines=use_scanlines)

class MyApp(Application):

    def setup(self):
        # show all characters in the default font
        x = y = 0
        for i in range(len(charset.data)):
            self.screen.setxy(x, y, fi, i, COLORS)
            x += 1
            if x >= self.screen.width:
                x = 0; y += 1

        self.update_screen()

    def handle_key(self, event):
        if event.key == 27: # Escape
            sys.exit()

print "Press Escape to exit."
app = MyApp(screen)
app.mainloop()

# In spite of the mainloop, this does NOT refresh/repaint the screen 'fps'
# times per second. It seems to just paint and then leave it alone (unlike
# Tkinter's canvas). A useful property to have.

