# spinners.py
#
# Has a weird bug: the regular text "Spinners with various speeds..." fades
# out, while the spinners stay as they are. Only happens when you use scanlines.
#
# Fading out would actually be another useful effect, but why is it happening
# here?? Even if I remove the spinners, it's fading out.

import os, sys
whereami = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.abspath(os.path.join(whereami, ".."))
sys.path.insert(0, parent_dir)

from retrokit import *

myfont = font.Font()
myfont.add(charsets.c64charset)

C = colors.RetroKitColors

fi = FontImageCollection(myfont, scale=2)

spinner_chars = ['horizontal-line', 'diagonal-backslash', 
                 'vertical-line', 'diagonal-slash']

spinner_data = [
    (1, 1, C.yellow, 60),
    (3, 1, C.red, 40),
    (5, 1, C.blue, 30),
    (7, 1, C.green, 15),
    (9, 1, C.white, 10),
    (11, 1, C.orange, 5),
    (13, 1, C.light_blue, 1),
]

class Spinner:
    def __init__(self, x, y, fg, speed):
        self.x, self.y = x, y
        self.image = fi.make_moving_image(spinner_chars, color(_=C.black, a=fg), 
                     speed)

spinners = [Spinner(x, y, fg, speed) for (x, y, fg, speed) in spinner_data]

use_scanlines = 'scanlines' in sys.argv[1:]


class MyApp(Application):

    def setup(self):
        self.screen.clear(fi, " ", color(_=C.black, a=C.gray))
        self.screen.writexy(1, 4, fi, color(_=C.black, a=C.gray), 
          "Spinners with various speeds...")
        for idx, c in enumerate("test test..."):
            self.screen.setxy(1+idx, 6, fi, c, color(_=C.black, a=C.blue))

        self.update_screen()

    def update(self):
        for spinner in spinners:
            spinner.image.tick()
            self.screen.setxy_direct(spinner.x, spinner.y, spinner.image)
        self.update_screen()

    def handle_key(self, event):
        if event.key == 27: # Escape
            sys.exit()

print "Press Escape to exit."
app = MyApp.with_screen(40, 25, scale=2, use_scanlines=use_scanlines)
app.mainloop()



