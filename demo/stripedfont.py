# stripedfont.py

import os, sys
whereami = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.abspath(os.path.join(whereami, ".."))
sys.path.insert(0, parent_dir)

from retrokit import *

stripedchars = charsets.ataricharset.copy()
for idx, char in enumerate(stripedchars.data):
    for i in (3,4): char[i] = char[i].replace("a", "b")
    for i in (5,6,7): char[i] = char[i].replace("a", "c")

myfont = font.Font()
myfont.add(stripedchars)

C = colors.RetroKitColors

fi = FontImageCollection(myfont, scale=2)

use_scanlines = 'scanlines' in sys.argv[1:]

class MyApp(Application):

    def setup(self):
        self.screen.clear(fi, " ", color(_=C.black, a=C.white))
        self.screen.writexy(1, 1, fi, color(_=C.black, a=C.light_gray,
            b=C.medium_gray, c=C.dark_gray), "Hello striped world!")
        self.screen.writexy(1, 3, fi, color(_=C.black, a=C.light_blue,
            b=C.blue, c=C.dark_blue), "Enjoy! Striped fonts!")
        self.screen.fill(0, 12, 39, 24, fi, " ", color(_=C.white, a=C.black))
        self.screen.writexy(1, 13, fi, color(_=C.white, a=C.light_gray,
            b=C.medium_gray, c=C.dark_gray), "Hello striped world!")
        self.screen.writexy(1, 15, fi, color(_=C.white, a=C.light_blue,
            b=C.blue, c=C.dark_blue), "Enjoy! Striped fonts!")
        self.update_screen()

    def handle_key(self, event):
        if event.key == 27: # Escape
            sys.exit()

print "Press Escape to exit."
app = MyApp.with_screen(40, 25, scale=2, use_scanlines=use_scanlines)
app.mainloop()



